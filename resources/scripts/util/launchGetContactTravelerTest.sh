#!/bin/bash

current_date=$(date +'%Y%m%d_%H%M%S')
STDOUT=$(echo "${HOME}/jmeter_benchmark_tests/test_logs/WCC_BenchmarkTest_B_GetContactTraveller_LeClub_${current_date}.csv")
STDERR=$(echo "${STDOUT}")
CMD_PATH=$(dirname $0)

echo "Test ongoing ..."

sed -i 's!\r!!g' "$1" # Clean Windows newline character if present

#rm "$STDOUT"

echo "request;time_namelookup;time_connect;time_appconnect;time_pretransfer;time_redirect;time_starttransfer;time_total" >>$STDOUT

IFSold=$IFS
IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing
for PMID in $(cat < "${1}"); do
  request_cmd="curl -w \"@${CMD_PATH}/curlformat.txt\" --tlsv1.2 -H \"Authorization: Basic d2NjLXN5bmM6cGFzc3dvcmQ=\" -o /dev/null -s \"${3}/api/v1/contacts/${PMID}\""
  echo "${request_cmd}" >>$STDOUT 2>>$STDERR &
  eval ${request_cmd} >>$STDOUT 2>>$STDERR &
  sleep ${2}
done

IFS=$IFSold		# Restitute previous separator character

# Format output logfile to have each request and its metrics on the same line as csv file  
sed -i -e ':a' -e 'N' -e '$!ba' -e 's!\n;!;!g' "$STDOUT"

echo "Log available at : ${STDOUT}"

echo "Test finished"
