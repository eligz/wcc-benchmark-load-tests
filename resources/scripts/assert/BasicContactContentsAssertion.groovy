import org.apache.jmeter.protocol.http.sampler.HTTPSampleResult;
//import org.apache.jmeter.protocol.http.sampler.HTTPSamplerProxy;
import java.text.Normalizer;
//import org.apache.commons.lang3.StringUtils

def replaceAccents(stringWithAccents){
    StringBuilder sb = new StringBuilder(stringWithAccents.length());
        stringWithAccents = Normalizer.normalize(stringWithAccents, Normalizer.Form.NFD);
        for (char c : stringWithAccents.toCharArray()) {
            if (c <= '\u007F') sb.append(c);
        }
        return sb.toString();
    //return Normalizer.normalize(stringWithAccents, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    //return Normalizer.normalize(stringWithAccents, Normalizer.Form.NFD).replaceAll("\\p{M}+", "");
    //return StringUtils.stripAccents(stringWithAccents);
}

//HTTPSamplerProxy previousSampler = ctx.getPreviousSampler();
//CookieManager cookieManager = previousSampler.getCookieManager();
HTTPSampleResult previousResult = (HTTPSampleResult)ctx.getPreviousResult();

// Sample BeanShell Assertion script
// Derived from http://www.mail-archive.com/jmeter-user@jakarta.apache.org/msg05597.html

if ( previousResult.getResponseCode().equals("404") )
{
    final String FailureMessage="the return code is " + previousResult.getResponseCode();
    final String DebugFailureMessage=FailureMessage + previousResult.getResponseMessage() + " for PMID["+vars.get("pmid")+"]" ;

    // this is standard stuff
    //AssertionResult.setFailure(true);
    //AssertionResult.setFailureMessage(FailureMessage);
    //OUT.println( DebugFailureMessage );   // this goes to stdout
    //log.warn( DebugFailureMessage ); // this goes to the JMeter log file
}
else if ( !previousResult.isResponseCodeOK() )
{
    final String FailureMessage="the return code is " + previousResult.getResponseCode();
    final String DebugFailureMessage=FailureMessage + previousResult.getResponseMessage() + " for PMID["+vars.get("pmid")+"]" ;

    // this is standard stuff
    AssertionResult.setFailure(true);
    AssertionResult.setFailureMessage(FailureMessage);
    OUT.println( DebugFailureMessage );   // this goes to stdout
    log.warn( DebugFailureMessage ); // this goes to the JMeter log file
} else {
    try
    {
        String FailureMessage="";
        String DebugFailureMessage="";
    		/*
         	// non standard stuff where BeanShell assertion will be really powerful .
         	// in my example I just test the size , but you could extend it further
         	// to actually test the content against another file.
         	byte [] arr = (byte[]) ResponseData ;
        	// print  ( arr.length ) ; // use this to determine the size
         	if (arr != null && arr.length != 25218)
         	{
             	AssertionResult.setFailure(true);
    			AssertionResult.setFailureMessage("The response data size was not as expected");
          }
          else if ( arr == null )
          {
              	AssertionResult.setFailure(true);
    			AssertionResult.setFailureMessage("The response data size was null");
          }
      		else {
      			
      		}*/

          //Locale.setDefault(new Locale("en_US"));
          final Boolean isMerged =   vars.get("firstname").toUpperCase().equals( "MERGED" ) && vars.get("lastname").toUpperCase().equals( "MERGED" ) \
                                  || vars.get("res_firstname").toUpperCase().equals( "MERGED" ) && vars.get("res_lastname").toUpperCase().equals( "MERGED" );
		
          if ( !isMerged && !vars.get("res_pmid").equals(vars.get("pmid")) ) {
          	FailureMessage+="PMID "; 
          	DebugFailureMessage+="PMID["+vars.get("pmid")+":"+vars.get("res_pmid")+"] ";}
          if ( vars.get("civility").size() > 0 && !vars.get("res_civility").toUpperCase().equals(vars.get("civility").toUpperCase()) ) {
          	FailureMessage+="civility "; 
          	DebugFailureMessage+="civility["+vars.get("civility")+":"+vars.get("res_civility")+"] ";}
          if ( !isMerged && vars.get("lastname").size() > 0 \
            && !replaceAccents( vars.get("res_lastname").toUpperCase() ).equals( replaceAccents( vars.get("lastname").toUpperCase() ) ) ) {
          	FailureMessage+="lastname ";
          	DebugFailureMessage+="lastname["+vars.get("lastname")+":"+vars.get("res_lastname")+"] ";}
          if ( !isMerged && vars.get("firstname").size() > 0 \
            && !replaceAccents( vars.get("res_firstname").toUpperCase() ).equals( replaceAccents( vars.get("firstname").toUpperCase() ) ) ) {
          	FailureMessage+="firstname "; 
          	DebugFailureMessage+="firstname["+vars.get("firstname")+":"+vars.get("res_firstname")+"] ";}          
          /*
          if ( vars.get("email").size() > 0 && !vars.get("res_email").toUpperCase().equals(vars.get("email").toUpperCase()) ) {
            FailureMessage+="email "; 
            DebugFailureMessage+="email["+vars.get("email")+":"+vars.get("res_email")+"] ";}
          */
          /*
          final String lastUpdateFormatted = vars.get("lastupd").replace("/","-").replace("T"," ")
          if ( !vars.get("res_lastupd").contains( lastUpdateFormatted ) ) {
            FailureMessage+="lastupd ";
            DebugFailureMessage+="lastupd["+lastUpdateFormatted+":"+vars.get("res_lastupd")+"] ";}
          */
          if ( !isMerged && vars.get("hasPassword") != null ) {
            final String hasPasswordFormatted = vars.get("hasPassword").replace("Y","true").replace("N","false")
            if ( hasPasswordFormatted.size() > 0 && ! vars.get("res_hasPassword").equals( hasPasswordFormatted ) ) {
              FailureMessage+="hasPassword ";
              DebugFailureMessage+="hasPassword["+hasPasswordFormatted+":"+vars.get("res_hasPassword")+"] ";}
          }
          /*
          if ( !isMerged && vars.get("isLeClubMember") != null ) {
            final String isLeClubMemberFormatted = vars.get("isLeClubMember").replace("Y","true").replace("N","false")
            if ( isLeClubMemberFormatted.size() > 0 && ! vars.get("res_isLeClubMember").equals( isLeClubMemberFormatted ) ) {
            	FailureMessage+="isLeClubMember ";
              DebugFailureMessage+="isLeClubMember["+isLeClubMemberFormatted+":"+vars.get("res_isLeClubMember")+"] ";}
          }
          */
          if ( FailureMessage.size() > 0 )
          {
          	 FailureMessage+=" do not match";
          	 DebugFailureMessage+=" do not match";
          	 AssertionResult.setFailure(true);
    			   AssertionResult.setFailureMessage(FailureMessage);
			       OUT.println(DebugFailureMessage);   // this goes to stdout
		    	   log.warn(DebugFailureMessage); // this goes to the JMeter log file
          }
     }
     catch ( Throwable t )
     {
          OUT.println( t ) ;
          log.warn("Error: ",t);
     }
}